<?php


function REAV_registerAveonline_generatePassword()
{
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 16; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
function REAV_registerAveonline_validatePost($att)
{
    $user = [];
    switch ($att["typeDocument"]) {
        case 'cc':
            $name = $att["spet-1-form-1-name"];
            if($name == ""){
                return array("status"=>"error","error"=>"Nombre invalido");
            }
            $lastName = $att["spet-1-form-1-lastName"];
            if($lastName == ""){
                return array("status"=>"error","error"=>"Apellidos invalido");
            }
            $numberDocument = $att["spet-1-form-1-numberDocument"];
            if($numberDocument == ""){
                return array("status"=>"error","error"=>"Numero de documento invalido");
            }
            $checkbox1 = $att["spet-1-form-1-checkbox1"];
            if($checkbox1 != "on"){
                return array("status"=>"error","error"=>"Al registrarte, aceptas la política de datos, cookies y términos y condiciones es invalid");
            }
            $email1 = $att["spet-2-form-1-email1"];
            if($email1 == ""){
                return array("status"=>"error","error"=>"Correo electronico invalido");
            }
            $typeMerchandise1 = $att["spet-2-form-1-typeMerchandise1"];
            if($typeMerchandise1 == ""){
                return array("status"=>"error","error"=>"Tipo de mercancia invalido");
            }
            $phone = $att["spet-2-form-1-phone"];
            if($phone == ""){
                return array("status"=>"error","error"=>"Celular Invalido");
            }
            $nameEmpre = $att["spet-2-form-1-nameEmpre"];
            if($nameEmpre == ""){
                return array("status"=>"error","error"=>"Nombre del emprendimiento invalido");
            }
            $city = $att["spet-2-form-1-city"];
            if($city == ""){
                return array("status"=>"error","error"=>"Ciudad invalida");
            }
            $billingAddress1 = $att["spet-2-form-1-billingAddress1"];
            if($billingAddress1 == ""){
                return array("status"=>"error","error"=>"Direccion de facturacion invalida");
            }
            $checkbox4 = $att["spet-2-form-1-checkbox4"];
            if($checkbox4 != "on"){
                return array("status"=>"error","error"=>"Al registrarte, aceptas la política de datos, cookies y términos y condiciones  invalida");
            }
            $user = array(
                "username" => $name,
                "user_email" => $email1,

                "custom_fileds" => array(
                    "name" => $name,
                    "user_nicename" => $name,
                    "first_name" => $name,
                    "last_name" => $lastName,
                    "typeDocument" => $typeDocument,
                    "numberDocument" => $numberDocument,
                    "phone" => $phone,
                    "name_enterprise" => $nameEmpre,
                    "city" => $city,
                    "address" => $billingAddress1,
                    "typeMerchandise1" => $typeMerchandise1,
                )
            );
            break;
        case 'nit':
            $nit = $att["spet-1-form-2-nit"];
            if($nit == ""){
                return array("status"=>"error","error"=>"Nit invalido");
            }
            $rs = $att["spet-1-form-2-rs"];
            if($rs == ""){
                return array("status"=>"error","error"=>"Razon Social invalida");
            }
            $checkbox2 = $att["spet-1-form-2-checkbox2"];
            if($checkbox2 != "on"){
                return array("status"=>"error","error"=>"Al registrarte, aceptas la política de datos, cookies y términos y condiciones invalida");
            }
            $nameContact = $att["spet-2-form-2-nameContact"];
            if($nameContact == ""){
                return array("status"=>"error","error"=>"Nombre de contacto invalido");
            }
            $phone1 = $att["spet-2-form-2-phone1"];
            if($phone1 == ""){
                return array("status"=>"error","error"=>"celular invalido");
            }
            $email2 = $att["spet-2-form-2-email2"];
            if($email2 == ""){
                return array("status"=>"error","error"=>"Correo electronico invalido");
            }
            $typeMerchandise2 = $att["spet-2-form-2-typeMerchandise2"];
            if($typeMerchandise2 == ""){
                return array("status"=>"error","error"=>"Tipo de mercancia invalido");
            }
            $city1 = $att["spet-2-form-2-city1"];
            if($city1 == ""){
                return array("status"=>"error","error"=>"ciudad invalida");
            }
            $billingAddress2 = $att["spet-2-form-2-billingAddress2"];
            if($billingAddress2 == ""){
                return array("status"=>"error","error"=>"Direccion de facturacion invalida");
            }
            $checkbox5 = $att["spet-2-form-2-checkbox5"];
            if($checkbox5 != "on"){
                return array("status"=>"error","error"=>"Al registrarte, aceptas la política de datos, cookies y términos y condiciones invalida");
            }
            $user = array(
                "username" => $nameContact,
                "user_email" => $email2,

                "custom_fileds" => array(
                    "name" => $nameContact,
                    "user_nicename" => $nameContact,
                    "first_name" => $nameContact,
                    "typeDocument" => $typeDocument,
                    "nit" => $nit,
                    "rs" => $rs,
                    "phone" => $phone1,
                    "typeCommodity" => $typeCommodity,
                    "city" => $city1,
                    "address" => $billingAddress2,
                    "typeMerchandise2" => $typeMerchandise2,
                )
            );
            break;
        case 'ce':
            $name = $att["spet-1-form-3-name"];
            if($name == ""){
                return array("status"=>"error","error"=>"Nombre invalido");
            }
            $lastName = $att["spet-1-form-3-lastName"];
            if($lastName == ""){
                return array("status"=>"error","error"=>"Apellidos invalido");
            }
            $numberDocument = $att["spet-1-form-3-numberDocument"];
            if($numberDocument == ""){
                return array("status"=>"error","error"=>"Numero de documento invalido");
            }
            $checkbox3 = $att["spet-1-form-3-checkbox3"];
            if($checkbox3 != "on"){
                return array("status"=>"error","error"=>"Al registrarte, aceptas la política de datos, cookies y términos y condiciones invalido");
            }
            $email3 = $att["spet-2-form-3-email3"];
            if($email3 == ""){
                return array("status"=>"error","error"=>"Correo electronico invalido");
            }
            $typeMerchandise3 = $att["spet-2-form-3-typeMerchandise3"];
            if($typeMerchandise3 == ""){
                return array("status"=>"error","error"=>"Tipo de mercancia invalido");
            }
            $phone2 = $att["spet-2-form-3-phone2"];
            if($phone2 == ""){
                return array("status"=>"error","error"=>"celular invalido");
            }
            $nameEmpre2 = $att["spet-2-form-3-nameEmpre2"];
            if($nameEmpre2 == ""){
                return array("status"=>"error","error"=>"Nombre del emprendimiento invalido");
            }
            $city2 = $att["spet-2-form-3-city2"];
            if($city2 == ""){
                return array("status"=>"error","error"=>"Ciudad invalida");
            }
            $billingAddress3 = $att["spet-2-form-3-billingAddress3"];
            if($billingAddress3 == ""){
                return array("status"=>"error","error"=>"Direccion de facturacion invalida");
            }
            $checkbox6 = $att["spet-2-form-3-checkbox6"];
            if($checkbox6 != "on"){
                return array("status"=>"error","error"=>"Al registrarte, aceptas la política de datos, cookies y términos y condiciones invalido");
            }
            break;
            $user = array(
                "username" => $name,
                "user_email" => $email3,

                "custom_fileds" => array(
                    "name" => $name,
                    "user_nicename" => $name,
                    "first_name" => $name,
                    "last_name" => $lastName,
                    "typeDocument" => $typeDocument,
                    "numberDocument" => $numberDocument,
                    "phone" => $phone2,
                    "name_enterprise" => $nameEmpre2,
                    "city" => $city2,
                    "address" => $billingAddress3,
                    "typeMerchandise3" => $typeMerchandise3,
                )
            );
        
        default:
            return array("status"=>"error","error"=>"Tipo de Documento Invalido");
            break;
    }

    $user["password"] = REAV_registerAveonline_generatePassword();

    return array(
        "status"=>"ok",
        "user"=>$user
    );
}


function REAV_registerAveonline() { 
    ob_start();

    if($_POST){
        //echo "<pre>";
       // var_dump($_POST);
        //echo "</pre>";
        $result = REAV_registerAveonline_validatePost($_POST);
        if($result["status"] == "ok"){
            echo "creamos usuario";
           // echo "<pre>";
            //var_dump($result["user"]);
           // echo "</pre>";

            $newUser = $result["user"];
            $result = wp_create_user($newUser["username"],$newUser["password"],$newUser["user_email"]);

            if(is_wp_error($result)){
                ?>
                <div class="REAV_registerAveonline_HTML_error">
                    <?=json_encode($result)?>
                </div>
                <?php
            }else{
                $user_id = $result;

                $custom_fileds = $newUser["custom_fileds"];

                foreach ($custom_fileds as $key => $value) {
                    update_user_meta($user_id,$key,$value);
                }
                wp_new_user_notification($user_id, $password);
                ?>
                <script>
                    window.location = "/entrar"
                </script>
                <?php

                return "";
            }
        }else{
            ?>
            <div class="REAV_registerAveonline_HTML_error">
                <?=$result["error"]?>
            </div>
            <?php
        }
    }

    REAV_registerAveonline_HTML();

    return ob_get_clean();
}
add_shortcode('REAV_registerAveonline', 'REAV_registerAveonline');