<?php

function REAV_registerAveonline_HTML() { 

    ?>
    <link rel="stylesheet" href="<?=REAV_URL?>src/css/REAV_registerAveonline.css?v=<?=REAV_get_version()?>">
    <form class="form-register-1" method="post">
        <input type="submit" hidden id="form-register-1-submit">
        <div id="step-1" class="content step step-1 active">
            <div class="contentSelectors" style="width:100%">
                <h1 class="content-title">
                    Registrate
                </h1>
                <span class="content-title-barr"></span>
                <h3 class="content-title-2">
                    Selecciona tu tipo de documento
                </h3>
                <div class="content-input list">
                    <div class="content-input-radio">
                        <label class="content-input-radio-label">
                            <input  name="typeDocument" value="cc" checked class="content-input-select" type="radio"   onclick="selectForm(1)">
                            CC
                        </label>
                    </div>
                    <div class="content-input-radio">
                        <label class="content-input-radio-label">
                            <input  name="typeDocument" value="nit"  class="content-input-select" type="radio"   onclick="selectForm(2)">
                            NIT
                        </label>
                    </div>
                    <div class="content-input-radio">
                        <label class="content-input-radio-label">
                            <input  name="typeDocument" value="ce"  class="content-input-select" type="radio"   onclick="selectForm(3)">
                            CE
                        </label>
                    </div>
                </div>
            </div>
            <div id="form-i-1" class="form-i active">
				<div class="content-input">
				   <div class="content-input-form">
						<label class="content-input-form-1">Nombre *</label>
						<input 
                            id="spet-1-form-1-name" 
                            name="spet-1-form-1-name" 
                            class="content-input-form-2" 
                            type="text"
                            onkeypress="removeCharacter(this,/([0-9]*)$/)"
                            onkeyup="removeCharacter(this,/([0-9]*)$/)"
                            onkeydown="removeCharacter(this,/([0-9]*)$/)"
                            onchange="removeCharacter(this,/([0-9]*)$/)"
                        />
				   </div>
				   <div class="content-input-form">
						<label class="content-input-form-1">Apellidos *</label>
						<input
                            id="spet-1-form-1-lastName"
                            name="spet-1-form-1-lastName"
                            class="content-input-form-2"
                            type="text"
                            onkeypress="removeCharacter(this,/([0-9]*)$/)"
                            onkeyup="removeCharacter(this,/([0-9]*)$/)"
                            onkeydown="removeCharacter(this,/([0-9]*)$/)"
                            onchange="removeCharacter(this,/([0-9]*)$/)"
                            >
				   </div>
				</div>
				<div class="content-input">
				   <div class="content-input-forms">
						<label class="content-input-form-1">Número de documento *</label>
						<input id="spet-1-form-1-numberDocument" name="spet-1-form-1-numberDocument" class="content-input-form-2" type="text">
				   </div>
				</div>
				<label class="content-input-checkbox">
					<input id="spet-1-form-1-checkbox1" name="spet-1-form-1-checkbox1" class="content-input-checkbox-sel" type="checkbox">
					<span class="content-input-checkbox-text">
						Al registrarte, aceptas la <a class="content-input-checkbox-text-link" href="https://aveonline.co/condiciones-politica-de-datos-y-cookies/">política de datos, cookies y términos y condiciones.</a>
					</span>
				</label>
                <div class="content-button">
                    <div class="content-button-step" onclick="stepNext()">SIGUIENTE</div>
                </div>
            </div>
            <div id="form-i-2" class="form-i">
				<div class="content-input">
				   <div class="content-input-form">
						<label class="content-input-form-1">NIT*</label>
						<input id="spet-1-form-2-nit" name="spet-1-form-2-nit" class="content-input-form-2" type="text">
				   </div>
				   <div class="content-input-form">
						<label class="content-input-form-1">Razon Social*</label>
						<input 
                        id="spet-1-form-2-rs" 
                        name="spet-1-form-2-rs" 
                        class="content-input-form-2" 
                        type="text"
                        onkeypress="removeCharacter(this,/([0-9]*)$/)"
                        onkeyup="removeCharacter(this,/([0-9]*)$/)"
                        onkeydown="removeCharacter(this,/([0-9]*)$/)"
                        onchange="removeCharacter(this,/([0-9]*)$/)"
                        >
				   </div>
				</div>
				<div class="content-input-checkbox">
					<input id="spet-1-form-2-checkbox2"  name="spet-1-form-2-checkbox2" class="content-input-checkbox-sel" type="checkbox">
					<span class="content-input-checkbox-text">
						Al registrarte, aceptas la <a class="content-input-checkbox-text-link" href="https://aveonline.co/condiciones-politica-de-datos-y-cookies/">política de datos, cookies y términos y condiciones.</a>
					</span>
				</div>
                <div class="content-button">
                    <div class="content-button-step" onclick="stepNext()">SIGUIENTE</div>
                </div>
            </div>
            <div id="form-i-3" class="form-i">
                <div class="content-input">
                    <div class="content-input-form">
                            <label class="content-input-form-1">Nombre *</label>
                            <input 
                            id="spet-1-form-3-name" 
                            name="spet-1-form-3-name" 
                            class="content-input-form-2" 
                            type="text"
                            onkeypress="removeCharacter(this,/([0-9]*)$/)"
                            onkeyup="removeCharacter(this,/([0-9]*)$/)"
                            onkeydown="removeCharacter(this,/([0-9]*)$/)"
                            onchange="removeCharacter(this,/([0-9]*)$/)"
                            >
                    </div>
                    <div class="content-input-form">
                            <label class="content-input-form-1">Apellidos *</label>
                            <input 
                            id="spet-1-form-3-lastName" 
                            name="spet-1-form-3-lastName" 
                            class="content-input-form-2" 
                            type="text"
                            onkeypress="removeCharacter(this,/([0-9]*)$/)"
                            onkeyup="removeCharacter(this,/([0-9]*)$/)"
                            onkeydown="removeCharacter(this,/([0-9]*)$/)"
                            onchange="removeCharacter(this,/([0-9]*)$/)"
                            >
                    </div>
                </div>
                <div class="content-input">
                    <div class="content-input-forms">
                            <label class="content-input-form-1">Número de documento *</label>
                            <input id="spet-1-form-3-numberDocument" name="spet-1-form-3-numberDocument" class="content-input-form-2" type="text">
                    </div>
                </div>
                <div class="content-input-checkbox">
                    <input id="spet-1-form-3-checkbox3" name="spet-1-form-3-checkbox3" class="content-input-checkbox-sel" type="checkbox">
                    <span class="content-input-checkbox-text">
                        Al registrarte, aceptas la <a class="content-input-checkbox-text-link" href="https://aveonline.co/condiciones-politica-de-datos-y-cookies/">política de datos, cookies y términos y condiciones.</a>
                    </span>
                </div>
                <div class="content-button">
                    <div class="content-button-step" onclick="stepNext()">SIGUIENTE</div>
                </div>
            </div>
        </div>
        <div id="step-2" class="step step-2">
            <div id="form-i-2-1" class="form-i form-i2 active">
                <div class="contentSelectors" style="width:100%">
                    <h1 class="content-title">
                        Registrate
                    </h1>
                    <span class="content-title-barr"></span>
                    <div class="content-title-notif">
                        <img class="content-title-notif-img" src="<?=REAV_URL?>src/img/circle.svg" alt="">
                        <h2 class="content-title-notif-text">
                            Debido a tu calificación, se habilitará pago contra entrega
                        </h2>
                    </div>
                    <h3 class="content-title-2">
                        Selecciona tu tipo de documento
                    </h3>
                    <div class="content-input list">
                        <div class="content-input-radio">
                            <label class="content-input-radio-label">
                                <input   value="cc" checked class="content-input-select" type="radio" disabled  >
                                CC
                            </label>
                        </div>
                        <div class="content-input-radio">
                            <label class="content-input-radio-label">
                                <input   value="nit"  class="content-input-select" type="radio" disabled  >
                                NIT
                            </label>
                        </div>
                        <div class="content-input-radio">
                            <label class="content-input-radio-label">
                                <input   value="ce"  class="content-input-select" type="radio" disabled  >
                                CE
                            </label>
                        </div>
                    </div>
                </div>
                <div class="content-input">
                    <div class="content-input-form">
                        <label class="content-input-form-1">Nombre *</label>
                        <input 
                        id="spet-2-form-1-name" 
                        name="spet-2-form-1-name" 
                        class="content-input-form-2" 
                        type="text" 
                        disabled
                        onkeypress="removeCharacter(this,/([0-9]*)$/)"
                            onkeyup="removeCharacter(this,/([0-9]*)$/)"
                            onkeydown="removeCharacter(this,/([0-9]*)$/)"
                            onchange="removeCharacter(this,/([0-9]*)$/)"
                        >
                    </div>
                    <div class="content-input-form">
                        <label class="content-input-form-1">Apellidos *</label>
                        <input 
                        id="spet-2-form-1-lastName" 
                        name="spet-2-form-1-lastName" 
                        class="content-input-form-2" 
                        type="text" 
                        disabled
                        onkeypress="removeCharacter(this,/([0-9]*)$/)"
                            onkeyup="removeCharacter(this,/([0-9]*)$/)"
                            onkeydown="removeCharacter(this,/([0-9]*)$/)"
                            onchange="removeCharacter(this,/([0-9]*)$/)"
                        >
                    </div>
                </div>
                <div class="content-input">
                    <div class="content-input-form">
                            <label class="content-input-form-1">Numero de documento *</label>
                            <input id="spet-2-form-1-numberDocument" name="spet-2-form-1-numberDocument" class="content-input-form-2" type="text" disabled>
                    </div>
                    <div class="content-input-form">
                            <label class="content-input-form-1">Celular *</label>
                            <input 
                            id="spet-2-form-1-phone" 
                            name="spet-2-form-1-phone" 
                            class="content-input-form-2" 
                            type="tel"
                            >
                    </div>
                </div>
                <div class="content-input">
                    <div class="content-input-form">
                        <label class="content-input-form-1">Correo electronico *</label>
                        <input id="spet-2-form-1-email1" name="spet-2-form-1-email1" class="content-input-form-2" type="email" >
                    </div>
                    <div class="content-input-form">
                        <label class="content-input-form-1">Tipo de mercancia *</label>
                        <input 
                        id="spet-2-form-1-typeMerchandise1" 
                        name="spet-2-form-1-typeMerchandise1" 
                        class="content-input-form-2" 
                        type="text"
                        onkeypress="removeCharacter(this,/([0-9]*)$/)"
                            onkeyup="removeCharacter(this,/([0-9]*)$/)"
                            onkeydown="removeCharacter(this,/([0-9]*)$/)"
                            onchange="removeCharacter(this,/([0-9]*)$/)"
                        >
                    </div>
                </div>
                <div class="content-input">
                    <div class="content-input-form">
                            <label class="content-input-form-1">Nombre del emprendimiento *</label>
                            <input 
                            id="spet-2-form-1-nameEmpre" 
                            name="spet-2-form-1-nameEmpre" 
                            class="content-input-form-2" 
                            type="text"
                            onkeypress="removeCharacter(this,/([0-9]*)$/)"
                            onkeyup="removeCharacter(this,/([0-9]*)$/)"
                            onkeydown="removeCharacter(this,/([0-9]*)$/)"
                            onchange="removeCharacter(this,/([0-9]*)$/)"
                            >
                    </div>
                    <div class="content-input-form">
                        <label class="content-input-form-1">Ciudad *</label>
                        <select id="spet-2-form-1-city" name="spet-2-form-1-city" class="content-input-form-3">
                            <option>Amazonas</option>
                            <option>Antioquia</option>
                            <option>Arauca</option>
                            <option>Atlántico</option>
                            <option>Bolívar</option>
                            <option>Boyacá</option>
                            <option>Caldas</option>
                            <option>Caquetá</option>
                            <option>Casanare</option>
                            <option>Cauca</option>
                            <option>Cesar</option>
                            <option>Chocó</option>
                            <option>Córdoba</option>
                            <option>Cundinamarca</option>
                            <option>Guainía</option>
                            <option>Guaviare</option>
                            <option>Huila</option>
                            <option>La Guajira</option>
                            <option>Magdalena</option>
                            <option>Meta</option>
                            <option>Nariño</option>
                            <option>Norte de Santander</option>
                            <option>Putumayo</option>
                            <option>Quindío</option>
                            <option>Risaralda</option>
                            <option>San Andrés y Providencia</option>
                            <option>Santander</option>
                            <option>Sucre</option>
                            <option>Tolima</option>
                            <option>Valle del Cauca</option>
                            <option>Vaupés</option>
                            <option>Vichada</option>
                        </select>
                    </div>
                </div>
                <div class="content-inputs">
                    <div class="content-input-form">
                        <label class="content-input-form-1">Direccion de facturacion *</label>
                        <input
                         id="spet-2-form-1-billingAddress1" 
                         name="spet-2-form-1-billingAddress1"
                          class="content-input-form-3" 
                          type="text"
                          
                          >
                    </div>
                </div>
                <div class="content-input-checkbox">
                    <input id="spet-2-form-1-checkbox4" name="spet-2-form-1-checkbox4"  class="content-input-checkbox-sel" type="checkbox">
                    <span class="content-input-checkbox-text">
                        Al registrarte, aceptas la <a class="content-input-checkbox-text-link" href="https://aveonline.co/condiciones-politica-de-datos-y-cookies/">política de datos, cookies y términos y condiciones.</a>
                    </span>
                </div>
                <div class="content-button">
                    <div class="content-button-step" onclick="stepNext()">REGISTRARME</div>
                </div>
                    <h1 class="content-title-account">
                        ¿Ya tienes una cuenta?
                    </h1>
                <span class="content-title-barr"></span>
                <div class="content-button-2">
                    <div class="content-button-init">REGISTRATE AHORA</div>
                </div>
            </div>
            <div id="form-i-2-2" class="form-i form-i2 ">
                <div class="contentSelectors" style="width:100%">
                    <h1 class="content-title">
                        Registrate
                    </h1>
                    <span class="content-title-barr"></span>
                    <div class="content-title-notif">
                        <img class="content-title-notif-img" src="<?=REAV_URL?>src/img/circle.svg" alt="">
                        <h2 class="content-title-notif-text">
                            Debido a tu calificación, se habilitará pago contra entrega
                        </h2>
                    </div>
                    <h3 class="content-title-2">
                        Selecciona tu tipo de documento
                    </h3>
                    <div class="content-input list">
                        <div class="content-input-radio">
                            <label class="content-input-radio-label">
                                <input   value="cc" checked class="content-input-select" type="radio" disabled  >
                                CC
                            </label>
                        </div>
                        <div class="content-input-radio">
                            <label class="content-input-radio-label">
                                <input   value="nit"  class="content-input-select" type="radio" disabled  >
                                NIT
                            </label>
                        </div>
                        <div class="content-input-radio">
                            <label class="content-input-radio-label">
                                <input   value="ce"  class="content-input-select" type="radio" disabled  >
                                CE
                            </label>
                        </div>
                    </div>
                </div>
                <div class="content-input">
                    <div class="content-input-form">
                            <label class="content-input-form-1">Razon Social*</label>
                            <input
                             id="spet-2-form-2-rs"
                              name="spet-2-form-2-rs"
                               class="content-input-form-2"
                                type="text" 
                                disabled>
                    </div>
                    <div class="content-input-form">
                        <label class="content-input-form-1">NIT*</label>
                        <input id="spet-2-form-2-nit" name="spet-2-form-2-nit" class="content-input-form-2" type="text" disabled>
                    </div>
                </div>
                <div class="content-input">
                    <div class="content-input-form">
                            <label class="content-input-form-1">Nombre de contacto *</label>
                            <input 
                            id="spet-2-form-2-nameContact"
                             name="spet-2-form-2-nameContact"
                              class="content-input-form-2"
                               type="text"
                               onkeypress="removeCharacter(this,/([0-9]*)$/)"
                            onkeyup="removeCharacter(this,/([0-9]*)$/)"
                            onkeydown="removeCharacter(this,/([0-9]*)$/)"
                            onchange="removeCharacter(this,/([0-9]*)$/)"
                               >
                    </div>
                    <div class="content-input-form">
                            <label class="content-input-form-1">Celular *</label>
                            <input id="spet-2-form-2-phone1" name="spet-2-form-2-phone1" class="content-input-form-2" type="tel">
                    </div>
                </div>
                <div class="content-input">
                    <div class="content-input-form">
                            <label class="content-input-form-1">Correo electronico *</label>
                            <input id="spet-2-form-2-email2" name="spet-2-form-2-email2" class="content-input-form-2" type="email" disabled>
                    </div>
                    <div class="content-input-form">
                            <label class="content-input-form-1">Tipo de mercancia *</label>
                            <input 
                            id="spet-2-form-2-typeMerchandise2" 
                            name="spet-2-form-2-typeMerchandise2"
                             class="content-input-form-2" 
                             type="text"
                             onkeypress="removeCharacter(this,/([0-9]*)$/)"
                            onkeyup="removeCharacter(this,/([0-9]*)$/)"
                            onkeydown="removeCharacter(this,/([0-9]*)$/)"
                            onchange="removeCharacter(this,/([0-9]*)$/)"
                             >
                    </div>
                </div>
                <div class="content-inputs">
                    <div class="content-input-form">
                        <label class="content-input-form-1">Ciudad *</label>
                        <select id="spet-2-form-2-city1" name="spet-2-form-2-city1" class="content-input-form-3">
                            <option>Amazonas</option>
                            <option>Antioquia</option>
                            <option>Arauca</option>
                            <option>Atlántico</option>
                            <option>Bolívar</option>
                            <option>Boyacá</option>
                            <option>Caldas</option>
                            <option>Caquetá</option>
                            <option>Casanare</option>
                            <option>Cauca</option>
                            <option>Cesar</option>
                            <option>Chocó</option>
                            <option>Córdoba</option>
                            <option>Cundinamarca</option>
                            <option>Guainía</option>
                            <option>Guaviare</option>
                            <option>Huila</option>
                            <option>La Guajira</option>
                            <option>Magdalena</option>
                            <option>Meta</option>
                            <option>Nariño</option>
                            <option>Norte de Santander</option>
                            <option>Putumayo</option>
                            <option>Quindío</option>
                            <option>Risaralda</option>
                            <option>San Andrés y Providencia</option>
                            <option>Santander</option>
                            <option>Sucre</option>
                            <option>Tolima</option>
                            <option>Valle del Cauca</option>
                            <option>Vaupés</option>
                            <option>Vichada</option>
                        </select>
                    </div>
                </div>
                <div class="content-inputs">
                    <div class="content-input-form">
                            <label class="content-input-form-1">Dirección facturación *</label>
                            <input id="spet-2-form-2-billingAddress2" name="spet-2-form-2-billingAddress2" class="content-input-form-3" type="text">
                    </div>
                </div>
                <div class="content-input-checkbox">
                    <input id="spet-2-form-2-checkbox5" name="spet-2-form-2-checkbox5" class="content-input-checkbox-sel" type="checkbox">
                    <span class="content-input-checkbox-text">
                        Al registrarte, aceptas la <a class="content-input-checkbox-text-link" href="https://aveonline.co/condiciones-politica-de-datos-y-cookies/">política de datos, cookies y términos y condiciones.</a>
                    </span>
                </div>
                <div class="content-button">
                    <div class="content-button-step" onclick="stepNext()">REGISTRARME</div>
                </div>
                    <h1 class="content-title-account">
                        ¿Ya tienes una cuenta?
                    </h1>
                <span class="content-title-barr"></span>
                <div class="content-button-2">
                    <div class="content-button-init">REGISTRATE AHORA</div>
                </div>
            </div>
            <div id="form-i-2-3" class="form-i form-i2 ">
                <div class="contentSelectors" style="width:100%">
                    <h1 class="content-title">
                        Registrate
                    </h1>
                    <span class="content-title-barr"></span>
                    <div class="content-title-notif">
                        <img class="content-title-notif-img" src="<?=REAV_URL?>src/img/circle.svg" alt="">
                        <h2 class="content-title-notif-text">
                            Debido a tu calificación, se habilitará pago contra entrega
                        </h2>
                    </div>
                    <h3 class="content-title-2">
                        Selecciona tu tipo de documento
                    </h3>
                    <div class="content-input list">
                        <div class="content-input-radio">
                            <label class="content-input-radio-label">
                                <input value="cc" checked class="content-input-select" type="radio" disabled  >
                                CC
                            </label>
                        </div>
                        <div class="content-input-radio">
                            <label class="content-input-radio-label">
                                <input   value="nit"  class="content-input-select" type="radio" disabled  >
                                NIT
                            </label>
                        </div>
                        <div class="content-input-radio">
                            <label class="content-input-radio-label">
                                <input  value="ce"  class="content-input-select" type="radio" disabled  >
                                CE
                            </label>
                        </div>
                    </div>
                </div>
                <div class="content-input">
                    <div class="content-input-form">
                        <label class="content-input-form-1">Nombre *</label>
                        <input 
                        id="spet-2-form-3-name"
                         name="spet-2-form-3-name"
                          class="content-input-form-2"
                           type="text" 
                           disabled>
                    </div>
                    <div class="content-input-form">
                        <label class="content-input-form-1">Apellidos *</label>
                        <input id="spet-2-form-3-lastName" name="spet-2-form-3-lastName" class="content-input-form-2" type="text" disabled>
                    </div>
                </div>
                <div class="content-input">
                    <div class="content-input-form">
                            <label class="content-input-form-1">Numero de documento *</label>
                            <input
                             id="spet-2-form-3-numberDocument"
                              name="spet-2-form-3-numberDocument" class="content-input-form-2" type="text" disabled>
                    </div>
                    <div class="content-input-form">
                            <label class="content-input-form-1">Celular *</label>
                            <input id="spet-2-form-3-phone2" name="spet-2-form-3-phone2" class="content-input-form-2" type="tel">
                    </div>
                </div>
                <div class="content-input">
                    <div class="content-input-form">
                            <label class="content-input-form-1">Correo electronico *</label>
                            <input id="spet-2-form-3-email3" name="spet-2-form-3-email3" class="content-input-form-2" type="email" >
                    </div>
                    <div class="content-input-form">
                            <label class="content-input-form-1">Tipo de mercancia *</label>
                            <input
                             id="spet-2-form-3-typeMerchandise3"
                              name="spet-2-form-3-typeMerchandise3"
                               class="content-input-form-2"
                                type="text"
                                onkeypress="removeCharacter(this,/([0-9]*)$/)"
                            onkeyup="removeCharacter(this,/([0-9]*)$/)"
                            onkeydown="removeCharacter(this,/([0-9]*)$/)"
                            onchange="removeCharacter(this,/([0-9]*)$/)" 
                                >
                    </div>
                </div>
                <div class="content-input">
                    <div class="content-input-form">
                            <label class="content-input-form-1">Nombre del emprendimiento *</label>
                            <input
                             id="spet-2-form-3-nameEmpre2" 
                             name="spet-2-form-3-nameEmpre2" 
                             class="content-input-form-2" 
                             type="text"
                             onkeypress="removeCharacter(this,/([0-9]*)$/)"
                            onkeyup="removeCharacter(this,/([0-9]*)$/)"
                            onkeydown="removeCharacter(this,/([0-9]*)$/)"
                            onchange="removeCharacter(this,/([0-9]*)$/)"
                             >
                    </div>
                    <div class="content-input-form">
                        <label class="content-input-form-1">Ciudad *</label>
                        <select id="spet-2-form-3-city2" name="spet-2-form-3-city2" class="content-input-form-3">
                            <option>Amazonas</option>
                            <option>Antioquia</option>
                            <option>Arauca</option>
                            <option>Atlántico</option>
                            <option>Bolívar</option>
                            <option>Boyacá</option>
                            <option>Caldas</option>
                            <option>Caquetá</option>
                            <option>Casanare</option>
                            <option>Cauca</option>
                            <option>Cesar</option>
                            <option>Chocó</option>
                            <option>Córdoba</option>
                            <option>Cundinamarca</option>
                            <option>Guainía</option>
                            <option>Guaviare</option>
                            <option>Huila</option>
                            <option>La Guajira</option>
                            <option>Magdalena</option>
                            <option>Meta</option>
                            <option>Nariño</option>
                            <option>Norte de Santander</option>
                            <option>Putumayo</option>
                            <option>Quindío</option>
                            <option>Risaralda</option>
                            <option>San Andrés y Providencia</option>
                            <option>Santander</option>
                            <option>Sucre</option>
                            <option>Tolima</option>
                            <option>Valle del Cauca</option>
                            <option>Vaupés</option>
                            <option>Vichada</option>
                        </select>
                    </div>
                </div>
                <div class="content-inputs">
                    <div class="content-input-form">
                        <label class="content-input-form-1">Direccion de facturacion *</label>
                        <input id="spet-2-form-3-billingAddress3" name="spet-2-form-3-billingAddress3" class="error content-input-form-3" type="text">
                    </div>
                </div>
                <div class="content-input-checkbox">
                    <input id="spet-2-form-3-checkbox6"  name="spet-2-form-3-checkbox6" class="content-input-checkbox-sel" type="checkbox">
                    <span class="content-input-checkbox-text">
                        Al registrarte, aceptas la <a class="content-input-checkbox-text-link" href="https://aveonline.co/condiciones-politica-de-datos-y-cookies/">política de datos, cookies y términos y condiciones.</a>
                    </span>
                </div>
                <div class="content-button">
                    <div class="content-button-step" onclick="stepNext()">REGISTRARME</div>
                </div>
                    <h1 class="content-title-account">
                        ¿Ya tienes una cuenta?
                    </h1>
                <span class="content-title-barr"></span>
                <div class="content-button-2">
                    <div class="content-button-init">REGISTRATE AHORA</div>
                </div>
            </div>
        </div>
    </form>
    <script src="<?=REAV_URL?>src/js/REAV_registerAveonline.js?v=<?=REAV_get_version()?>"></script>
    <?php
    }