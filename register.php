<?php
/*
Plugin Name: Register Aveonline
Plugin URI: https://gitlab.com/luism23/registeraveoline
Description: Register Aveonline
Author: Aveonline
Version: 1.0.14
Author URI: https://gitlab.com/luism23
License: GPL
 */


require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/registeraveoline',
	__FILE__,
	'registeraveoline'
);


function REAV_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}
define("REAV_LOG",true);
define("REAV_PATH",plugin_dir_path(__FILE__));
define("REAV_URL",plugin_dir_url(__FILE__));

require_once REAV_PATH . "src/_index.php";